import {LAMUDI_JIRA_DOMAIN} from "./config.js";
import {TabService} from "./tab.service.js";

export class IssueHelper
{
  constructor() {
    this.TabService = new TabService();
  }

  async getJiraLinkAndTitleFromDom() {
    return this.dispatchGetJiraLinkAndTitleRequest().then((response) => {
      response.key_from_url = response.url_from_window.replace(`${LAMUDI_JIRA_DOMAIN}/browse/`, '');

      return response;
    });
  }

  async dispatchGetJiraLinkAndTitleRequest() {
    return this.TabService.executeScriptInCurrentTab(
      function() {
        return {
          url_from_window: window.location.href,
          key_from_dom: document.querySelector('[data-test-id="issue.views.issue-base.foundation.breadcrumbs.breadcrumb-current-issue-container"] a').textContent,
          summary_from_dom: document.querySelector('[data-test-id="issue.views.issue-base.foundation.summary.heading"]')
            .textContent,
        }
      }
    ).then(response => {
      return response[0].result;
    });
  }
}