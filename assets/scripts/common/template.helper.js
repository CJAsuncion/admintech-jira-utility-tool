export class TemplateHelper
{
  constructor() {
  }
  /**
   * @param html {String} HTML representing a single element
   * @return {Element}
   */
  htmlToElement(html) {
    html = html.trim(); // Never return a text node of whitespace as the result
    let template = document.createElement('template');
    template.innerHTML = html;
    return template.content.firstChild;
  }

  /**
   * @param html {String} HTML representing any number of sibling elements
   * @return {NodeList}
   */
  htmlToElements(html) {
    html = html.trim()
    let template = document.createElement('template');
    template.innerHTML = html;
    return template.content.childNodes;
  }
}