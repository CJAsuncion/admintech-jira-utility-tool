export class TabService {
  executeScriptInCurrentTab(scriptFunction) {
    return new Promise((resolve, reject) => {
      if (typeof scriptFunction == 'function') {
        this.getCurrentTab().then(tab => {
          chrome.scripting.executeScript({
            target: {tabId: tab.id, allFrames: true},
            function: scriptFunction,
          }, function (response) {
            resolve(response);
          });
        })
      }
    });
  }

  async getCurrentTab() {
    const [currentTab] = await chrome.tabs.query({active: true, currentWindow: true});
    return currentTab;
  }
}

let tabService = new TabService();