export class ClipboardService
{
  copyText(text) {
    let tempElementsHolder = document.getElementById('temp_elements_holder');
    var textArea = document.createElement("textarea");
    textArea.value = text;
    tempElementsHolder.appendChild(textArea);

    textArea.focus();
    textArea.select();

    var successCopyFlag = document.execCommand('copy');
    tempElementsHolder.removeChild(textArea);
  }

  copyLink(href, text) {
    const tempElementsHolder = document.getElementById('temp_elements_holder');
    const tempLink = document.createElement('a');
    tempLink.style.background = "transparent";
    tempLink.style.backgroundColor = "transparent";
    tempLink.style.color = "initial";
    tempLink.href = href;
    tempLink.text = text;
    tempElementsHolder.appendChild(tempLink);

    const range = document.createRange();
    range.selectNode(tempLink);
    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);

    const success = document.execCommand('copy');
    tempElementsHolder.removeChild(tempLink);
  }
}