export class LogService
{
  logMessage(message) {
    if (typeof message === "object") {
      message = JSON.stringify(message, undefined, 2);
    }

    document.getElementById('log_bar').innerHTML = message;
  }
}