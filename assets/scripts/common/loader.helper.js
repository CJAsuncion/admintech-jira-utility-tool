export class LoaderHelper
{
  appendLoader(container, description) {
    this.removeLoader(container);

    let loader = document.createElement("div");
    loader.className = "loader lds-dual-ring";
    container.appendChild(loader);
    container.style.cursor = "progress";
  }

  removeLoader(container) {
    let differentLoader = container.querySelector(".loader");

    if (differentLoader) {
      do {
        container.removeChild(differentLoader);
        differentLoader = container.querySelector(".loader");
      } while (differentLoader);
    }

    container.style.removeProperty("cursor");
  }
}