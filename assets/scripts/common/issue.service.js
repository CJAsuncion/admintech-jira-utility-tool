import {LAMUDI_JIRA_DOMAIN} from "./config.js";

export class IssueService
{
    recursionDepth = 2;

    constructor() {
        this.getConfig();
    }

    async getConfig() {
        await chrome.storage.sync.get([
            "recursion_depth",
        ], (value) => {
            this.recursionDepth = value.recursion_depth;
        });
    }

    getRecursionDepth() {
        return this.recursionDepth;
    }

    async asyncGetEpicIssues(epicKey) {
        let url = LAMUDI_JIRA_DOMAIN + `/rest/api/3/search?jql="Epic Link"=${epicKey}`;

        return await fetch(url, {
            method: 'GET'
        }).then(response => response.json());
    }

    async asyncGetIssuesWithoutDescription() {
      let apiEndPoint = LAMUDI_JIRA_DOMAIN + `/rest/api/3/search`
      let jql = `status in ("Code Review", Denied, Development, "Merged in RC", "PO Review", QA, "Ready for PO Review", "Ready for QA", "Ready to merge") AND labels = admin_tech AND description IS NULL order by created DESC`
      let url = apiEndPoint + '?jql=' + jql;

      return await fetch(url, {
        method: 'GET'
      }).then(response => response.json());
    };

    /**
     * @param issueKey
     * @returns issuePromose = Promise
     */
    getIssue(issueKey) {
        console.log('getIssue')
        let issuePromise = new Promise(function(resolve, reject) {
            var xmlHttp = new XMLHttpRequest();
            var url = "https://lamuditech.atlassian.net/rest/api/latest/issue/" + issueKey;

            xmlHttp.open("GET", url);

            xmlHttp.onload = function() {
                if (this.readyState === 4 && this.status === 200) {
                    resolve(JSON.parse(xmlHttp.responseText));
                } else {
                    reject(JSON.parse(xmlHttp.statusText));
                }
            }

            xmlHttp.send();
        });

        return issuePromise;
    }

    recursivelyGetIssuesAndSubtasks(issues, recurseCount) {
        return new Promise((resolve, reject) => {
            var allIssuesPromise = [];

            issues.forEach(issue => {
                var issueKey = issue.key;
                allIssuesPromise.push(this.getIssue(issueKey));
            });

            Promise.all(allIssuesPromise).then(allIssuesPromise => {
                if (recurseCount == 1) {
                    console.log('Parent Issues', allIssuesPromise);
                    this.log('Retrieving Parent Issues');
                }

                if (recurseCount == 0) {
                    resolve(allIssuesPromise);
                } else {
                    recurseCount--;
                    var issuesWithCompleteSubtasks = [];
                    var allIssuesSubtasksPromise = [];

                    allIssuesPromise.forEach(issuePromiseResponse => {
                        let msg1 = 'Retrieving Subtasks of ' + issuePromiseResponse.key + ' : ' + issuePromiseResponse.fields.summary
                        console.log(msg1);

                        var issueSubtasks = JSON.parse(JSON.stringify(issuePromiseResponse.fields.subtasks));

                        var issueAllSubtasksPromise = this.recursivelyGetIssuesAndSubtasks(issueSubtasks, recurseCount);
                        allIssuesSubtasksPromise.push(issueAllSubtasksPromise);

                        var issueWihCompleteSubtasks = JSON.parse(JSON.stringify(issuePromiseResponse));

                        issueAllSubtasksPromise.then(issueAllSubtasksPromiseResponse => {
                            let issueMsg = 'Retrieved Subtasks of ' + issuePromiseResponse.key + ' : ' + issuePromiseResponse.fields.summary;
                            console.log(issueMsg, issueAllSubtasksPromiseResponse);
                            this.log(issueMsg);
                            issueWihCompleteSubtasks.fields.subtasks = issueAllSubtasksPromiseResponse;
                            issuesWithCompleteSubtasks.push(issueWihCompleteSubtasks);
                        });
                    });

                    Promise.all(allIssuesSubtasksPromise).then(allIssuesSubtasksPromiseResponse => {
                        console.log('issuesWithCompleteSubtasks', issuesWithCompleteSubtasks)
                        resolve(issuesWithCompleteSubtasks);
                    });
                }
            });
        });
    }

    sortIssues(arrOfOrderedIssues, arrOfUndeorderedIssues) {
        var orderedIssues = [];

        arrOfOrderedIssues.forEach(orderedIssue => {
            var orderedIssueKey = orderedIssue.key;

            arrOfUndeorderedIssues.forEach(unorderedIssue => {
                var unorderedIssueKey = unorderedIssue.key;

                if (orderedIssueKey === unorderedIssueKey) {
                    orderedIssues.push(unorderedIssue);
                }
            });
        });

        return orderedIssues;
    }

    /**
     *
     * @param issuesList
     * @returns issueListText = Comma and tab-delimited issue key and issue summary
     */
    generateParentAndSubtasksListForSprintTaskBreakdown(issuesList) {
        var issueListText = "";

        issuesList.forEach(issue => {
            var parentIssue = issue.key + "\t" + issue.fields.summary + "\n";
            issueListText += parentIssue;

            issue.fields.subtasks.forEach(subtask => {
                var hours = Math.floor(subtask.fields.aggregatetimeoriginalestimate / 3600);
                var minutes = (subtask.fields.aggregatetimeoriginalestimate % 3600) / 60;
                issueListText += "\t" + subtask.fields.summary + "\t" + (hours) + "\t" + minutes + "\n";
            });

            issueListText += "\n\n";
        });

        return issueListText;
    }

    log(message) {
        let msgContainer = document.getElementById('message_container');
        msgContainer.innerText = message;
    }
}
