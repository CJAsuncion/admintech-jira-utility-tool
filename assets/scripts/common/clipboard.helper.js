import {ClipboardService} from "./clipboard.service.js";

export class ClipboardHelper
{
  constructor() {
    this.ClipboardService = new ClipboardService();
  }

  copyArrayOfObject(arrayOfObject) {
    let text = "";

    arrayOfObject.forEach((object, objectIndex, objectArray) => {
      Object.keys(object).map((objectKey, index, array) => {
        text += object[objectKey];

        if (index === array.length - 1) {
          return;
        }

        text += "\t"
      });

      if (objectIndex === objectArray.length - 1) {
        return;
      }

      text += "\n";
    });

    this.ClipboardService.copyText(text);
  }

  recursivelyCopyArrayOfObject(arrayOfObject) {

  }
}