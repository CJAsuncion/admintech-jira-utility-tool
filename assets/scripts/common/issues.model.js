export class IssuesModel
{
  constructor(issuesArray) {
    this.issues = issuesArray;
  }

  getIssuesKeyAndFieldSummary() {
    let response = [];

    this.issues.forEach((issue) => {
      let obj = {
        key: issue.key,
        summary: issue.fields.summary,
      };
      response.push(obj);
    });

    return response;
  }

  getIssuesKeyAndFieldSummaryAndFieldStatusName() {
    let response = [];

    this.issues.forEach((issue) => {
      let obj = {
        key: issue.key,
        summary: issue.fields.summary,
        status: issue.fields.status.name,
      };
      response.push(obj);
    });

    return response;
  }
}