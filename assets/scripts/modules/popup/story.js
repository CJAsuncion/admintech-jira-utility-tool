import {IssueService} from "../../common/issue.service.js";
import {LogService} from "../../common/log.service.js";
import {IssueHelper} from "../../common/issue.helper.js";
import {IssuesModel} from "../../common/issues.model.js";
import {ClipboardHelper} from "../../common/clipboard.helper.js";
import {LoaderHelper} from "../../common/loader.helper.js";

class Story {

  constructor() {
    this.ClipboardHelper = new ClipboardHelper();
    this.IssueService = new IssueService();
    this.IssueHelper = new IssueHelper();
    this.LoaderHelper = new LoaderHelper();
    this.LogService = new LogService();

    this.init();
  }

  init() {
    this.setInitialSubTasksStatusFormValues();
    this.attachSubTasksStatusFormEventHandler();
  }

  setInitialSubTasksStatusFormValues() {
    this.IssueHelper.getJiraLinkAndTitleFromDom().then((response) => {
      let issueKey = response.key_from_url;
      document.querySelector('#story_key').textContent = issueKey

      this.IssueService.getIssue(issueKey).then((issueResponse) => {
        let issueType = issueResponse.fields.issuetype.name;

        switch (issueType) {
          case "Story":
          case "Sub-task":
            document.querySelector('#story_task_ticket_type').setAttribute('checked', true);
            break;
          case "Epic":
            document.querySelector('#epic_ticket_type').setAttribute('checked', true);
            break;
        }
      });
    });
  }

  attachSubTasksStatusFormEventHandler() {
    let subTasksStatusForm = document.querySelector('[name=sub_tasks_status_form]');
    let getSubTasksButton = document.getElementById('get_subtasks_status');

    subTasksStatusForm.addEventListener("submit", (event) => {
      event.preventDefault();
      getSubTasksButton.textContent = "Processing...";
      this.LoaderHelper.appendLoader(getSubTasksButton);

      let subTasksStatusFormData = new FormData(subTasksStatusForm);
      let storyKeyTextAreaVal = subTasksStatusFormData.get("story_key").replace(" ", "");
      let issueKeysArray = storyKeyTextAreaVal.split(",")
      let ticketType = subTasksStatusFormData.get("ticket_type");

      switch (ticketType) {
        case "story_task":
          console.log('story_task');
          break;
        case "epic":
          this.IssueService.asyncGetEpicIssues('PP-2285').then(res => {
            this.LogService.logMessage(JSON.stringify(res, undefined, 2));
            console.log('epic', res);
          })
          break;
        default:
          break;
      }

      /**
       *
       */
      issueKeysArray.forEach((storyKey) => {
        this.IssueService.getIssue(storyKey).then(issue => {
          let issueSubTasks = issue.fields.subtasks;
          let issuesModel = new IssuesModel(issueSubTasks);

          const ticketStatus = subTasksStatusFormData.get("ticket_status");

          let issuesKeysAndFieldSummaryArrayObject;

          if (ticketStatus === 'on') {
            issuesKeysAndFieldSummaryArrayObject = issuesModel.getIssuesKeyAndFieldSummaryAndFieldStatusName();
          } else {
            issuesKeysAndFieldSummaryArrayObject = issuesModel.getIssuesKeyAndFieldSummary();
          }

          this.ClipboardHelper.copyArrayOfObject(issuesKeysAndFieldSummaryArrayObject);
          this.LoaderHelper.removeLoader(getSubTasksButton);
          getSubTasksButton.innerHTML = "Copied to clipboard &#10003;";
          // setTimeout(() => {
          //   getSubTasksButton.textContent = "Get story sub-tasks";
          // }, 1000)
        })
      });
    });
  }
}

let subtasks = new Story();
