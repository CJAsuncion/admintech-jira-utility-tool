import {ClipboardService} from "../../common/clipboard.service.js";
import {IssueHelper} from "../../common/issue.helper.js";
import {IssueService} from "../../common/issue.service.js";
import {LoaderHelper} from "../../common/loader.helper.js";
import {TemplateHelper} from "../../common/template.helper.js";

export class QuickTools {
  constructor() {
    this.ClipboardService = new ClipboardService();
    this.IssueHelper = new IssueHelper();
    this.IssueService = new IssueService();
    this.LoaderHelper = new LoaderHelper();
    this.TemplateHelper = new TemplateHelper();

    this.setCopyLinkAndTitleButtonLabel();
    this.attachCopyLinkAndTitleEventListener();
    this.attachDisplayTicketsWithoutDescription();
  }

  setCopyLinkAndTitleButtonLabel() {
    setTimeout(() => {
      this.IssueHelper.getJiraLinkAndTitleFromDom()
        .then(response => {
          document.querySelector('#copy_link_and_title .line-clamp')
            .innerHTML = `Copy "<a href="${response.url_from_window}">[${response.key_from_dom}] ${response.summary_from_dom}</a>" link`;
          // document.querySelector('#copy_link_and_title .line-clamp')
          //   .textContent = `Copy as link [${response.key_from_dom}] ${response.summary_from_dom}`;
        });
    })
  }

  attachCopyLinkAndTitleEventListener() {
    let copyLinkAndTitleButton = document.getElementById('copy_link_and_title');
    copyLinkAndTitleButton.addEventListener("click", () => {
      this.copyJiraLinkAndTitle();
    });
  }

  copyJiraLinkAndTitle() {
    this.IssueHelper.getJiraLinkAndTitleFromDom()
      .then(response => {
        let url = response.url_from_window;
        let summary = `[${response.key_from_dom}] ${response.summary_from_dom}`;
        this.ClipboardService.copyLink(url, summary);
      });
  }

  attachDisplayTicketsWithoutDescription() {
    let displayTicketsWithoutDescriptionButton = document.getElementById('display_tickets_without_description');
    displayTicketsWithoutDescriptionButton.addEventListener("click", () => {
      this.displayTicketsWithoutDescription();
    });
  }

  displayTicketsWithoutDescription() {
    let tempElementsHolder = document.getElementById("temp_elements_holder");
    tempElementsHolder.innerHTML = "";
    let displayTicketsWithoutDescriptionButton = document.querySelector("#display_tickets_without_description .line-clamp");
    displayTicketsWithoutDescriptionButton.textContent = "Searching...";
    this.LoaderHelper.appendLoader(displayTicketsWithoutDescriptionButton);

    this.IssueService.asyncGetIssuesWithoutDescription()
      .then(response => {
        response.issues.forEach((issue) => {
          let issueItemTemplate = `
          <a href="https://lamuditech.atlassian.net/browse/${issue.key}" class="issue-item-container" title="${issue.fields.summary}">
            <span class="key">${issue.key}</span>
            <span class="summary line-clamp">${issue.fields.summary}</span>
            <span class="assignee">
                <img class="avatar" src="${issue.fields?.assignee?.avatarUrls?.['48x48'] ?? issue.fields?.reporter?.avatarUrls?.['48x48']}" alt="${issue.fields.assignee.displayName}">
            </span>
          </a>        
        `;
          let issueItem = this.TemplateHelper.htmlToElements(issueItemTemplate)[0];
          tempElementsHolder.append(issueItem);
        });


        this.LoaderHelper.removeLoader(displayTicketsWithoutDescriptionButton);
        displayTicketsWithoutDescriptionButton.textContent = "Display tickets w/o description";
      });
  }
}

let quickTools = new QuickTools();