class FixVersions
{
    constructor() {
        this.init();
    }

    init() {
        this.getPersistFixVersionsAndIssueKeys();
        this.trackPersistFieldValues();
        this.getFixVersions();
    }

    trackPersistFieldValues() {
        let fields = document.getElementsByClassName('js-persist-value');

        Array.from(fields).forEach(field => {
            field.addEventListener('change', (e) => {
                const id = e.target.id;
                const value = e.target.value;
                this.persistField(id, value);
            })
        });
    }

    persistField(key, value) {
        let obj = {};
        obj[key] = value;

        chrome.storage.sync.set(obj, function() {
            console.log(obj);
        });
    }

    getPersistFixVersionsAndIssueKeys() {
        chrome.storage.sync.get(["fix_versions", "issue_keys"], function(value) {
            document.getElementById('fix_versions').value = value.fix_versions ?? '';
            document.getElementById('issue_keys').value = value.issue_keys ?? '';
        });
    }

    getFixVersions() {
        let _this = this;
        const func = function () {
            return new Promise(resolve => {
                const options = {
                    'method': 'GET',
                    'headers': {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                };

                fetch('https://lamuditech.atlassian.net/rest/api/2/issue/PP-2003?expand=editmeta&fields=fixVersions', options).then(response => {
                    //resolve(response.json());
                    resolve('X');
                });
            });
        };

        this.executeScriptInCurrentTab(func).then(res => {
            console.log('res', res);
        })
    }

    executeScriptInCurrentTab(scriptFunction) {
        return new Promise((resolve, reject) => {
            if (typeof scriptFunction == 'function') {
                this.getCurrentTab().then(tab => {
                    chrome.scripting.executeScript({
                        target: {tabId: tab.id, allFrames: true},
                        function: scriptFunction,
                    }, function (response) {
                        response.then(promiseResponse => {
                            resolve(promiseResponse);
                        })
                    });
                })
            }
        });
    }

    async getCurrentTab() {
        const [currentTab] = await chrome.tabs.query({active: true, currentWindow: true});
        return currentTab;
    }

    async _get(endpoint, queryParams) {
        const options = {
            'method': 'GET',
            'headers': {
                'Accept': 'application/json'
            }
        }
        const queryString = queryParams ? Object.keys(queryParams).map(key => key + '=' + queryParams[key]).join('&') : '';
        const url = endpoint + (queryString.length ? `?${queryString}` : '');

        const response = await fetch(url, options);
        return response.json();
    }

    async _post(endpoint, body) {
        const authorizationHeader = `Basic ${this._getBasicAuth()}`;
        const options = {
            'method': 'POST',
            'headers': {
                'Accept': 'application/json',
                'Authorization': authorizationHeader,
                'Content-Type': 'application/json',
            }
        };
        body && (options.body = JSON.stringify(body));

        const response = await fetch(endpoint, options);
        return response.json();
    }

    _getBasicAuth() {
        const email = 'jay.asuncion@lamudi.com';
        const apiToken = 'getYourOwnToken';
        const plainText = `${email}:${apiToken}`;

        return btoa(plainText);
    }
    /**
     * https://lamuditech.atlassian.net/gateway/api/fields/v2/recommendations/ POST
     * {fieldId: "fixVersions"}
     *
     * https://lamuditech.atlassian.net/rest/api/2/issue/PP-1977 PUT
     * {"fields":{"fixVersions":[{"name":"PropertyPRO Admin 8.2.0","id":"10649","fromCache":false}]}}
      */

}

var popup = new FixVersions();
