const sprint_group_input = document.querySelector('#sprint_group_selector');
const sprint_input = document.querySelector('#sprint_selector');
const sprint_issue_input = document.querySelector('#sprint_issue_selector');

const recursion_depth_input = document.querySelector('#recursion_depth');

const apply_btn = document.querySelector('#apply_btn');

init();

function init() {
    chrome.storage.sync.get(["sprint_group_selector", "sprint_selector", "sprint_issue_selector", "recursion_depth"], function(value) {
        sprint_group_input.value = value.sprint_group_selector;
        sprint_input.value = value.sprint_selector;
        sprint_issue_input.value = value.sprint_issue_selector;
        recursion_depth_input.value = value.recursion_depth;

        apply_btn.addEventListener('click',applyButtonHandler);
    });
}

function applyButtonHandler() {
    const sprint_group_selector = sprint_group_input.value;
    const sprint_selector = sprint_input.value;
    const sprint_issue_selector = sprint_issue_input.value;

    const recursion_depth = recursion_depth_input.value;

    chrome.storage.sync.set({
        'sprint_group_selector': sprint_group_selector,
        'sprint_selector': sprint_selector,
        'sprint_issue_selector': sprint_issue_selector,
        'recursion_depth': recursion_depth
    }, function() {
        alert("Settings Saved Successfully ")
    });
}
